extends Node3D

var speed = 8.0
var dir = Vector3(0,0,1)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	transform.origin += dir.normalized()*speed*delta
