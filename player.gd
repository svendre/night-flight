extends CharacterBody3D


const SPEED = 5.0
const JUMP_VELOCITY = 4.5
const GRAVITY = 2.5
var maxSpeed = Vector3(2,2,0)
var points = 0
var dead = false
var played = false
var snow = false
signal ground


func _physics_process(delta):
	if !dead:
		if not Input.is_action_pressed("ui_accept"):
			velocity.y -= GRAVITY * delta
		else:
			velocity.y += JUMP_VELOCITY * delta

		var input_dir = Input.get_vector("ui_left", "ui_right", "ui_left", "ui_right")
		var direction = (transform.basis * Vector3(input_dir.x, 0, 0)).normalized()
		if direction:
			velocity.x += direction.x * SPEED * delta
		else:
			velocity.x = move_toward(velocity.x, 0, delta)
		velocity = velocity.clamp(-maxSpeed,maxSpeed)
		
		
		if position.y < 1.25:
			ground.emit()
			dead = true
	else:
		if !played:
			played = true
			$AudioStreamPlayer.playing=true
			
		velocity.y -= GRAVITY * delta
	
	if position.y < 1.25 && !snow:
		snow = true
		$snow.play()
	move_and_slide()

func _on_area_3d_area_entered(area):
	$point.play()
	area.queue_free()



func _on_label_no_health():
	dead = true
