extends Label

var points = 0
var life = 3
signal noHealth

func gameover():
	$gameover.visible = true

func _on_area_3d_area_entered(area):
	points += 1
	text = str(points)

func _on_deadline_area_entered(area):
	if life>0:
		$miss.play()
	life -= 1
	if life == 2:
		$l2.queue_free()
	elif life == 1:
		$l3.queue_free()
	elif life == 0:
		$l1.queue_free()
		gameover()
		noHealth.emit()


func _on_audio_stream_player_finished():
	$AudioStreamPlayer.play()


func _on_player_ground():
	if life == 3:
		$l3.queue_free()
		$l1.queue_free()
		$l2.queue_free()
	if life == 2:
		$l1.queue_free()
		$l3.queue_free()
	if life == 1:
		$l1.queue_free()
	life = 0
	gameover()


func _on_button_pressed():
	get_tree().reload_current_scene()
