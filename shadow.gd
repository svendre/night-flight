extends MeshInstance3D

var player
# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_parent().get_node("player")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.x = player.position.x
	var size = 0
	var pos = player.position.y
	if pos < 1.25:
		size = 0
	elif pos < 3:
		size = (pos - 3)/2
	scale = Vector3(size,size,1)
