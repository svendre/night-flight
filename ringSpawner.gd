extends Node3D

var width = 1
var height = 3.5
@onready var timer = $Timer
@export_category("Rings")
@export var ringSmall: Resource
@export var ringMedium: Resource
@export var ringLarge: Resource

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	spawn_rings()
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func _on_timer_timeout():
	spawn_rings()
	
func spawn_rings():
	var instance = ringMedium.instantiate()
	instance.transform.origin = Vector3(randf_range(-width,width),randf_range(-height,height),0)
	
	add_child(instance)
	
